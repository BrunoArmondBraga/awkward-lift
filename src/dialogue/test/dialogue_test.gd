extends Node3D


# Called when the node enters the scene tree for the first time.
func _ready():
	var speech_balloon = load("res://dialogue/balloon.tscn")
	var instance = speech_balloon.instantiate()
	instance.set_text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
	add_child(instance)

