extends Node3D
class_name SpeechBalloon

const SPEED: float = 0.05

func set_text(text: String):
	$Pivot.visible = false
	$Pivot/Text.text = text
	if text.length() > 55:
		$Pivot/Balloon.scale.y = 0.3
	elif text.length() > 80:
		$Pivot/Balloon.scale.y = 0.4
	$AnimationPlayer.play("fade_in")
	$Timer.wait_time = text.length() * SPEED
	await $Timer.timeout
	$AnimationPlayer.play_backwards("fade_in")
