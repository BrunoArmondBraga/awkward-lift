extends Area3D

@export var interactable: Node # Who will implement the interaction result

var enabled: bool = true

@onready var connections := {
	"mouse_entered": _on_mouse_entered,
	"mouse_exited": _on_mouse_exited,
	"input_event": _on_input_event,
}

func _ready():
	for signal_name in connections:
		connect(signal_name, connections[signal_name])

func enable():
	enabled = true

func disable():
	enabled = false

# Called when the mouse enters the clickable area
func _on_mouse_entered():
	if enabled:
		Input.set_default_cursor_shape(Input.CURSOR_POINTING_HAND)

# Called when the mouse leaves the clickable area
func _on_mouse_exited():
	Input.set_default_cursor_shape(Input.CURSOR_ARROW)

# Called when an input event involving the clickable area happens (hopefully a click)
func _on_input_event(_camera, event: InputEvent, _position, _normal3, _shape_idx):
	if (enabled and event is InputEventMouseButton and event.pressed):
		if interactable.has_method("interact"):
			interactable.interact()
