extends Marker3D


func create_speech(text: String):
	var speech_balloon = load("res://dialogue/balloon.tscn")
	var instance = speech_balloon.instantiate()
	add_child(instance)
	instance.set_text(text)
