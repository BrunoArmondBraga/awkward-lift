#@tool
extends Node3D

const BODY_PARTS_PATHS = {
	BodyParts.NOSE: "res://assets/character/noses/nose_%s.glb",
	BodyParts.HAIR: "res://assets/character/hair/hair_%s.glb",
}

const BODY_PARTS_VARIATIONS = {
	BodyParts.NOSE: 4,
	BodyParts.HAIR: 3,
}

const EXPRESSIONS_PATHS = {
	Expressions.NEUTRAL: "res://assets/character/expressions/neutral.png",
	Expressions.MAD: "res://assets/character/expressions/mad.png",
	Expressions.FURIOUS: "res://assets/character/expressions/furious.png",
}

@export var skin_gradient: Gradient
@export var hair_gradient: Gradient
@export var cloth_gradient: Gradient

enum BodyParts {
	NOSE,
	HAIR
}

enum Expressions {
	NEUTRAL,
	MAD,
	FURIOUS
}

func _ready():
	generate_random_appearance()
	change_facial_expression(Expressions.NEUTRAL)
	#while true:
	#	remove_child(get_child(2))
	#	remove_child(get_child(3))
	#	generate_random_appearance()
	#	change_facial_expression(Expressions.values()[randi() % Expressions.size()])
	#	await get_tree().create_timer(2).timeout


func change_facial_expressions_to_mad() -> void:
	change_facial_expression(Expressions.MAD)


func change_facial_expressions_to_furious() -> void:
	change_facial_expression(Expressions.FURIOUS)


func generate_random_appearance() -> void:
	var random_offset: float = randf()
	var skin_color: Color = skin_gradient.sample(random_offset)
	set_albedo($Head, skin_color)
	
	var darker_skin_color: Color = skin_gradient.sample(random_offset - 0.1)
	set_random_body_part(BodyParts.NOSE, darker_skin_color)
	
	set_random_body_part(BodyParts.HAIR, hair_gradient.sample(randf()))
	
	set_albedo($Body, cloth_gradient.sample(randf()))


func set_albedo(mesh: MeshInstance3D, color: Color) -> void:
	mesh.mesh.surface_get_material(0).albedo_color = color


func set_random_body_part(part: BodyParts, color: Color) -> void:
	var packed: Node3D = get_random_variation(part)
	set_albedo(packed.get_child(0), color)
	add_child(packed)


func get_random_variation(part: BodyParts) -> Node3D:
	var variation: int = randi_range(1, BODY_PARTS_VARIATIONS[part])
	var packed: Node3D = load(BODY_PARTS_PATHS[part] % variation).instantiate()
	return packed


func change_facial_expression(expression: Expressions) -> void:
	var face_material = $Head.mesh.surface_get_material(0).next_pass
	face_material.albedo_texture = load(EXPRESSIONS_PATHS[expression])
