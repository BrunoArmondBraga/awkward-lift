extends Node3D

var hurry: bool
var introvert: bool = false
@export var patience: int = 1000

var current_state: States = States.NORMAL
@export var mad_threshold: int = 600
@export var furious_threshold: int = 300

@export var patience_time_loop: int = 4
@export var floor_penalty: int = 100
@export var patience_gain: int = 25

@export var desired_floor: int = 1
var velocity: float = 5.0

var normal_to_mad_transitions := ["I dont have time for this", "I need to get going", "Come on, I've got places to be", "Come on, seriously? This is starting to test my patience."]
var mad_to_furious_transitions := ["This is ridiculous! Get out of the way now!","What's the holdup? I don't have time for this nonsense", "Move it! I've had enough of being delayed!", "This is infuriating! Just when I thought it couldn't get worse."]

enum States {
	NORMAL,
	MAD,
	FURIOUS
}


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	hurry = randf() > 0.5
	if(!hurry):
		introvert = randf() > 0.5


func generate_desired_floor(prevented_floor: int) -> void:
	var random_floor: int = prevented_floor
	
	while(random_floor == prevented_floor):
		random_floor = randi_range(-1,8)
	
	desired_floor = random_floor
	print("my desired floor is " + str(desired_floor))


func update_state() -> void:
	if patience >= mad_threshold:
		current_state = States.NORMAL
	elif patience > furious_threshold:
		if(current_state != States.MAD):
			current_state = States.MAD
			$Appearance.change_facial_expressions_to_mad()
			transition_talk(0)
	else:
		if(current_state != States.FURIOUS):
			current_state = States.FURIOUS
			$Appearance.change_facial_expressions_to_furious()
			transition_talk(1)


func enter_elevator(pos: Vector3) -> void:
	self.talk()
	await walk(pos)
	get_bonus_patience()


func leave_elevator(positions: Array) -> void:
	var i: int = 0
	for position in positions:
		if(i != 0):
			await walk(position.global_position)
		i += 1
	queue_free()


func walk(pos: Vector3) -> void:
	var tween
	tween = create_tween()
	#print(pos - self.global_position)
	var duration = (self.global_position.distance_to(pos)) / velocity
	#print("duration = " + str(duration))
	rotate_for_walk(self.global_position, pos)
	fix_rotation()
	tween.tween_property(self, "global_position", pos, duration)
	await tween.finished


func rotate_for_walk(before: Vector3, after: Vector3) -> void:
	self.look_at(after)
	return

func fix_rotation() -> void:
	self.rotation.x = self.rotation.x - PI/2
	self.rotation.y = self.rotation.y - PI/2


func get_desired_floor() -> int:
	return desired_floor


func _on_patience_timer_timeout() -> void:
	patience -= 10
	print("my patience is " + str(patience))
	update_state()


func get_bonus_patience() -> void:
	patience += patience_gain


func check_floor_penalty(floor_index: int) -> void:
	if(floor_index == desired_floor):
		patience -= floor_penalty

func talk() -> void:
	$DialogueCreator.create_speech(str(desired_floor) + "th floor please!")


func transition_talk(state: int) -> void:
	if(state == 0):
		$DialogueCreator.create_speech(normal_to_mad_transitions[randi_range(0,3)])
	else:
		$DialogueCreator.create_speech(mad_to_furious_transitions[randi_range(0,3)])
