extends Control

@export_file("*.tscn") var next_scene_path

func _on_continue_pressed():
	$AnimationPlayer.play_backwards("slide_in")
	$FadeAnimationPlayer.play("fade_out")
	await $FadeAnimationPlayer.animation_finished
	get_tree().change_scene_to_file(next_scene_path)


func _on_quit_pressed():
	get_tree().quit()
