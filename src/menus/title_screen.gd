extends Control

@export_file("*.tscn") var next_scene_path

func _ready():
	$AnimationPlayer.play_backwards("fade")


func _on_play_pressed():
	$AnimationPlayer.play("fade")
	await $AnimationPlayer.animation_finished
	get_tree().change_scene_to_file(next_scene_path)


func _on_quit_pressed():
	get_tree().quit()
