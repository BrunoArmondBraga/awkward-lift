extends Node3D
class_name Elevator

signal door_opened
signal door_closed
signal floor_changed(new_floor: int)
signal direction_changed(new_direction: int)

@onready var animation_player = $AnimationPlayer
var current_floor: int = 0
var is_closed: bool = true
var direction: int = 0

@export var floor_manager: Node3D

func _ready():
	connect("door_opened", func(): is_closed = false)
	connect("door_closed", func(): is_closed = true)
	var root_node = get_tree().get_root()


func open_doors() -> void:
	$StateMachine.transition_to("Opening")


func close_doors() -> void:
	$StateMachine.transition_to("Closing")


func go_to(target_floor: int) -> void:
	if not is_closed:
		$StateMachine.transition_to("Closing", {"move_after": true, "target_floor": target_floor})
	else:
		$StateMachine.transition_to("Move", {"target_floor": target_floor})
	await door_opened
	await handle_passengers(target_floor)


func handle_passengers(floor_index: int) -> void:
	var current_floor_node = floor_manager.get_children()[floor_index + 1]
	await $PassengersSpots.handle_passengers(current_floor_node, floor_index)


func update_floor(amount: int) -> void:
	floor_patience_penalty(current_floor)
	current_floor += amount
	floor_changed.emit(current_floor)


func floor_patience_penalty(floor_index: int) -> void:
	$PassengersSpots.floor_penalty(floor_index)


func set_direction(dir: int) -> void:
	direction = dir
	direction_changed.emit(direction)


func get_floor_distance() -> int:
	return floor_manager.floor_distance

func highlight_floor(floor_index: int) -> void:
	$Panel.highlight_yellow(floor_index)
