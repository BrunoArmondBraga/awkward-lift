extends Node3D

@export var positions_array : Array

func _ready():
	for pos in range(5):
		positions_array.append(false)


func get_position_from_index(pos: int) -> Vector3:
	return self.get_children()[pos].global_position


func get_next_empty_position() -> int:
	var count: int = 0
	for pos in positions_array:
		if pos == false:
			positions_array[count] = true
			print("count = " + str(count))
			return count
		count += 1
	return -1


func get_number_of_empty_positions() -> int:
	var count: int = 0
	for pos in positions_array:
		if pos == false:
			count += 1
	return count


func get_elevator_center() -> Vector3:
	return Vector3.ZERO


func handle_passengers(floor: Node3D, floor_index: int) -> void:	
	var number_of_passenger = floor.get_number_of_passengers_entering()
	var number_of_empty_positions = get_number_of_empty_positions()
	
	if(number_of_passenger == 0 and number_of_empty_positions == 0):
		return
	
	unbind_passengers(floor, floor_index)
	var children: Array = floor.get_passengers_entering()
	
	for child in children:
		if(number_of_empty_positions == 0):
			return
		number_of_empty_positions -= 1
		var next_position = get_next_empty_position()
		print(number_of_empty_positions)
		await bind_passenger(child, next_position, get_position_from_index(next_position))
		floor.subtract_passenger()


func bind_passenger(passenger: Node3D, position: int, final_pos: Vector3) -> void:
	await passenger.enter_elevator(final_pos)
	var actual_father = self.get_children()[position]
	if passenger.get_parent():
		passenger.get_parent().remove_child(passenger)
	actual_father.add_child(passenger)
	passenger.position = Vector3.ZERO


func unbind_passengers(floor_node: Node3D, floor_index: int) -> void:
	var count: int = 0
	for child in self.get_children():
		var children: Array = child.get_children()
		if(children.size() != 0):
			print("my desired floor is: " + str(children[0].get_desired_floor()))
			if(children[0].get_desired_floor() == floor_index):
				var plan = floor_node.get_exit_plan()
				children[0].leave_elevator(plan)
				await get_tree().create_timer(1.0).timeout
				positions_array[count] = false
		count += 1


func floor_penalty(floor_index: int) -> void:
	for child in self.get_children():
		var children: Array = child.get_children()
		if(children.size() != 0):
			children[0].check_floor_penalty(floor_index)
