extends Node3D

@export var floor_distance: int = 10

@export var elevator: Node3D

@export var spawn_number = 5
@export var current_spawn_number = 0
var passenger_scene = preload("res://passenger/passenger.tscn")
var array_of_floors = [1, 2, 3, 4, 5, 6, 7, 8, 9]
#var array_of_floors = [1, 2, 3]
var female_names := ["Mary", "Jennifer", "Karen", "Sarah", "Suzan", "Megan", "Heather", "Kimberly", "Amy", "Emily"]
var male_names := ["James", "Robert", "Michael", "David", "Richard", "Charles", "Paul", "Mark", "Anthony", "Donald"]

var spawn_index: int = 0
@export var start_cooldown: float = 3
@export var normal_cooldown: float = 5.0

func _ready() -> void:
	array_of_floors.shuffle()
	$SpawnTimer.wait_time = start_cooldown


func _on_spawn_timer_timeout() -> void:
	var passenger_instance = passenger_scene.instantiate()
	passenger_instance.generate_desired_floor(array_of_floors[spawn_index] - 1)
	self.get_children()[array_of_floors[spawn_index]].add_passenger(passenger_instance)
	print("Spawned one passenger in floor: " + str(array_of_floors[spawn_index] - 1))
	highlight_floor(array_of_floors[spawn_index] - 1)
	
	spawn_index += 1
	current_spawn_number += 1
	
	if(spawn_index >= array_of_floors.size()):
		spawn_index = 0
		array_of_floors.shuffle()
	
	
	if(spawn_index > 2):
		$SpawnTimer.wait_time = normal_cooldown
	if(current_spawn_number == spawn_number):
		$SpawnTimer.set_paused(true)

func highlight_floor(number_index: int) -> void:
	elevator.highlight_floor(number_index)
