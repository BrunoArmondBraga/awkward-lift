extends Node
class_name ElevatorState

var state_machine = null
var elevator: Elevator


func _ready() -> void:
	await (owner.ready)
	elevator = owner as Elevator
	assert(elevator != null)


func handle_input(_event: InputEvent) -> void:
	pass


func update(delta: float) -> void:
	pass


func physics_update(delta: float) -> void:
	pass


func enter(_msg := {}) -> void:
	pass


func exit() -> void:
	pass
