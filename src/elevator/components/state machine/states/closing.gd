extends ElevatorState


func enter(_msg := {}) -> void:
	var animation_player: AnimationPlayer = elevator.animation_player
	animation_player.play_backwards("open_doors")
	await animation_player.animation_finished
	elevator.emit_signal("door_closed")
	
	if _msg.has("move_after"):
		state_machine.transition_to("Move", {"target_floor" : _msg["target_floor"]})
	else:
		state_machine.transition_to("Idle")


func update(delta: float) -> void:
	pass


func physics_update(delta: float) -> void:
	pass


func exit() -> void:
	pass
