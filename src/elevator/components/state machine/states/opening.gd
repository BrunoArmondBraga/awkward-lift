extends ElevatorState


func enter(_msg := {}) -> void:
	var animation_player: AnimationPlayer = elevator.animation_player
	animation_player.play("open_doors")
	await animation_player.animation_finished
	elevator.emit_signal("door_opened")
	state_machine.transition_to("Idle")


func update(delta: float) -> void:
	pass


func physics_update(delta: float) -> void:
	pass


func exit() -> void:
	pass
