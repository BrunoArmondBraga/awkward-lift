extends ElevatorState


func update(delta: float) -> void:
	pass


func physics_update(delta: float) -> void:
	if Input.is_action_pressed("ui_up"):
		state_machine.transition_to("Move", {"target_floor" : 2})
	if Input.is_action_pressed("ui_down"):
		state_machine.transition_to("Move", {"target_floor" : -1})


func enter(_msg := {}) -> void:
	pass


func exit() -> void:
	pass
