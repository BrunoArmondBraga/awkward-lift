extends ElevatorState

const DURATION = 1
var tween


func update(delta: float) -> void:
	pass


func physics_update(delta: float) -> void:
	pass


func move(direction: int, target_floor: int) -> void:
	elevator.set_direction(direction)
	while elevator.current_floor != target_floor:
		if tween:
			tween.kill()
		tween = create_tween()
		tween.tween_property(elevator, "position:y", \
		elevator.position.y + direction * elevator.get_floor_distance(), DURATION)
		await tween.finished
		elevator.update_floor(direction)
	elevator.set_direction(0)
	state_machine.transition_to("Opening")


func enter(_msg := {}) -> void:
	var target_floor: int = _msg["target_floor"]
	if not elevator.is_closed:
		state_machine.transition_to("Closing", {"move_after": true, "target_floor": target_floor})
		return
	var direction: int = sign(target_floor - elevator.current_floor)
	move(direction, target_floor)


func exit() -> void:
	pass
