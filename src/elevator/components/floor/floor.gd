extends Node3D

@export var number_of_passengers_entering: int = 0

func get_number_of_passengers_entering() -> int:
	return number_of_passengers_entering


func get_passengers_entering() -> Array:
	return $Passengers.get_children()


func get_next_passenger() -> Node3D:
	var passengers = $Passengers
	return passengers.get_children()[0]


func add_passenger(passenger: Node3D) -> void:
	$Passengers.add_child(passenger)
	passenger.transform = $Markers.get_children()[0].transform
	number_of_passengers_entering += 1


func subtract_passenger() -> void:
	number_of_passengers_entering -= 1


func get_exit_plan() -> Array:
	var exitplan: Array
	var markers = $Markers.get_children()
	exitplan.append(markers[1])
	exitplan.append(markers[2])
	var random_exit = randf()
	if(random_exit < 0.5):
		exitplan.append(markers[3])
	else:
		exitplan.append(markers[4])
	return exitplan
