extends Node3D

@export var elevator: Elevator

const DIRECTIONS = {
	-1: "↓",
	0: " ",
	1: "↑",
}

func _ready():
	update_display()
	elevator.connect("floor_changed", update_display)
	elevator.connect("direction_changed", update_display)


func update_display(_arg = null):
	print(elevator.direction)
	$Display.text = DIRECTIONS[elevator.direction] + str(elevator.current_floor)


func disable_buttons():
	for child in $Buttons.get_children():
		child.disable()


func enable_buttons(exception: int):
	for child: FloorButton in $Buttons.get_children():
		if child.floor != exception:
			child.enable()


func go_to(target_floor: int):
	get_button(target_floor).highlight_red()
	disable_buttons()
	await elevator.go_to(target_floor)
	get_button(target_floor).unhighlight()
	enable_buttons(elevator.current_floor)


func get_button(floor: int):
	return $Buttons.get_child(floor + 1)


func highlight_yellow(floor: int):
	get_button(floor).highlight_yellow()
