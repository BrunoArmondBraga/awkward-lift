@tool
extends Node3D
class_name FloorButton

@export var panel: Node3D
@export var floor: int

func _ready():
	$Label3D.text = str(floor)

func interact():
	panel.go_to(floor)

func enable():
	$Clickable.enable()

func disable():
	$Clickable.disable()

func highlight_red():
	$Mesh.mesh.surface_get_material(0).emission_enabled = true
	$Mesh.mesh.surface_get_material(0).emission = Color(150, 0, 0)

func highlight_yellow():
	$Mesh.mesh.surface_get_material(0).emission_enabled = true
	$Mesh.mesh.surface_get_material(0).emission = Color(155, 115, 0)

func unhighlight():
	$Mesh.mesh.surface_get_material(0).emission_enabled = false
